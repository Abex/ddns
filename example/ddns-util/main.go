package main

import (
	"flag"
	"fmt"
	"net"
	"time"

	"bitbucket.org/abex/ddns"
)

func main() {
	server := flag.String("server", "", "Required: Nameserver including port. ex: ns.google.com:53")
	root := flag.String("root", "", "Required: Root zone FQDN including trailing dot. ex: google.com.")
	keyname := flag.String("keyname", "", "Required: Name of the key used to connect as a FQDN (trailing dot)")
	key := flag.String("key", "", "Required: Key in base64 format")
	keyalgo := flag.String("keyalgo", "hmac-md5", "Required: Algorithm for key. Can be hmac-md5, hmac-sha1, hmac-sha256, hmac-sha512")

	name := flag.String("name", "", "Subdomain to be created as a FQDN including trailing dot. ex: test.google.com.")
	ip := flag.String("ip", "", "IP address for the A or AAAA record")
	ttl := flag.Duration("ttl", time.Minute*5, "How long can recursive nameservers cache queries")
	timeout := flag.Duration("timeout", time.Hour*72, "How long until the record will be deleted")

	flag.Parse()

	if *server == "" || *root == "" || *keyname == "" || *key == "" || *keyalgo == "" {
		flag.PrintDefaults()
		return
	}
	client, err := ddns.MakeClient(*server, *root, *keyname, *key, *keyalgo)
	if err != nil {
		panic(err)
	}

	if *name == "" || *ip == "" {
		fmt.Println("Missing argument for update/insert, cleanup only")
	} else {
		iip := net.ParseIP(*ip)
		if iip == nil {
			fmt.Printf("'%v' is not a valid ip address.", *ip)
			return
		}
		err = client.Add(ddns.Record{
			Name:    *name,
			IP:      iip,
			Timeout: time.Now().Add(*timeout),
			TTL:     *ttl,
		})
		if err != nil {
			panic(err)
		}
	}
	err = client.Cleanup()
	if err != nil {
		panic(err)
	}
}
