package main

import (
	"flag"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"net/http"

	"bitbucket.org/abex/ddns"
)

func main() {
	server := flag.String("server", "", "Required: Nameserver including port. ex: ns.google.com:53")
	root := flag.String("root", "", "Required: Root zone FQDN including trailing dot. ex: google.com.")
	keyname := flag.String("keyname", "", "Required: Name of the key used to connect as a FQDN (trailing dot)")
	key := flag.String("key", "", "Required: Key in base64 format")
	keyalgo := flag.String("keyalgo", "hmac-md5", "Required: Algorithm for key. Can be hmac-md5, hmac-sha1, hmac-sha256, hmac-sha512")
	addr := flag.String("host", ":8080", "Address to listen on")
	timeout := flag.Duration("timeout", time.Hour*72, "How long until the record will be deleted")
	realip := flag.String("realip", "", "Header to get actual IP address (reverse proxy)")

	flag.Parse()

	if *server == "" || *root == "" || *keyname == "" || *key == "" || *keyalgo == "" {
		flag.PrintDefaults()
		return
	}
	client, err := ddns.MakeClient(*server, *root, *keyname, *key, *keyalgo)
	if err != nil {
		panic(err)
	}
	go func() {
		time.Sleep(time.Minute * 5)
		client.Cleanup()
	}()

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		err := req.ParseForm()
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
		name := req.Form.Get("name")
		sttl := req.Form.Get("ttl")
		host := req.Form.Get("host")
		if host == "" {
			if *realip == "" {
				host = req.RemoteAddr
			} else {
				host = req.Header.Get(*realip)
			}
		}
		lhc := strings.LastIndexByte(host, ':')
		if lhc != -1 && lhc > strings.LastIndexByte(host, ']') {
			host = host[:lhc]
		}
		if host[0] == '[' && host[len(host)-1] == ']' {
			host = host[1 : len(host)-1]
		}
		ttl, _ := strconv.Atoi(sttl)
		if name == "" || host == "" || ttl <= 0 {
			w.WriteHeader(400)
			w.Write([]byte("Missing parameters"))
			return
		}
		iip := net.ParseIP(host)
		if iip == nil {
			w.WriteHeader(400)
			w.Write([]byte(fmt.Sprintf("'%v' is not a valid ip address.", host)))
			return
		}
		err = client.Add(ddns.Record{
			Name:    fmt.Sprintf("%v.%v", name, *root),
			IP:      iip,
			Timeout: time.Now().Add(*timeout),
			TTL:     time.Second * time.Duration(ttl),
		})
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte(err.Error()))
			return
		}
		w.WriteHeader(200)
		w.Write([]byte("Success!"))
	})

	http.ListenAndServe(*addr, nil)
}
