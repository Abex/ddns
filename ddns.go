package ddns

import (
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/miekg/dns"
)

type Client struct {
	root    string
	host    string
	key     string
	keyname string
	algo    string
}

func MakeClient(hostname, root, keyname, key, algo string) (*Client, error) {
	c := &Client{
		root:    root,
		host:    hostname,
		key:     key,
		keyname: keyname,
	}
	switch strings.ToLower(algo) {
	case "hmac-md5", "md5":
		c.algo = dns.HmacMD5
	case "hmac-sha1", "sha1":
		c.algo = dns.HmacSHA1
	case "hmac-sha256", "sha256":
		c.algo = dns.HmacSHA256
	case "hmac-sha512", "sha512":
		c.algo = dns.HmacSHA512
	default:
		return nil, fmt.Errorf("Unknown alogrithm: %v", algo)
	}
	return c, nil
}

type Record struct {
	Name    string
	IP      net.IP
	Timeout time.Time
	TTL     time.Duration
}

func (c *Client) Add(r Record) error {
	cnt := &dns.Client{
		Net:        "tcp",
		TsigSecret: map[string]string{c.keyname: c.key},
	}
	m := &dns.Msg{}
	m.SetUpdate(c.root)
	m.RemoveRRset([]dns.RR{
		&dns.ANY{dns.RR_Header{
			Name:   r.Name,
			Rrtype: dns.TypeTXT,
		}},
		&dns.ANY{dns.RR_Header{
			Name:   r.Name,
			Rrtype: dns.TypeA,
		}},
		&dns.ANY{dns.RR_Header{
			Name:   r.Name,
			Rrtype: dns.TypeAAAA,
		}},
	})
	m.Insert([]dns.RR{
		&dns.TXT{
			Hdr: dns.RR_Header{
				Name:   r.Name,
				Rrtype: dns.TypeTXT,
				Class:  dns.ClassINET,
				Ttl:    uint32(r.TTL.Seconds()),
			},
			Txt: []string{
				fmt.Sprintf("expire=%X", r.Timeout.Unix()),
			},
		},
	})
	ipv4 := r.IP.To4()
	if ipv4 == nil {
		m.Insert([]dns.RR{
			&dns.AAAA{
				Hdr: dns.RR_Header{
					Name:   r.Name,
					Rrtype: dns.TypeAAAA,
					Class:  dns.ClassINET,
					Ttl:    uint32(r.TTL.Seconds()),
				},
				AAAA: r.IP,
			},
		})
	} else {
		m.Insert([]dns.RR{
			&dns.A{
				Hdr: dns.RR_Header{
					Name:   r.Name,
					Rrtype: dns.TypeA,
					Class:  dns.ClassINET,
					Ttl:    uint32(r.TTL.Seconds()),
				},
				A: ipv4,
			},
		})
	}
	m.SetTsig(c.keyname, c.algo, 300, time.Now().Unix())
	_, _, err := cnt.Exchange(m, c.host)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Cleanup() error {
	m := &dns.Msg{}
	t := &dns.Transfer{}
	t.TsigSecret = map[string]string{c.keyname: c.key}
	m.SetAxfr(c.root)
	m.SetTsig(c.keyname, c.algo, 300, time.Now().Unix())
	e, err := t.In(m, c.host)
	if err != nil {
		return err
	}
	del := []dns.RR{}
	now := time.Now().Unix()
	for r := range e {
		if r.Error != nil {
			return r.Error
		}
		for _, rr := range r.RR {
			txt, ok := rr.(*dns.TXT)
			name := rr.Header().Name
			if ok {
				for _, t := range txt.Txt {
					i := strings.IndexByte(t, '=')
					if i != -1 && t[:i] == "expire" {
						i, err := strconv.ParseInt(t[i+1:], 16, 64)
						if err != nil || i < now {
							del = append(del, &dns.ANY{Hdr: dns.RR_Header{Name: name}})
						}
					}
				}
			}
		}
	}
	if len(del) > 0 {
		cnt := &dns.Client{
			Net:        "tcp",
			TsigSecret: map[string]string{c.keyname: c.key},
		}
		m := &dns.Msg{}
		m.SetUpdate(c.root)
		m.RemoveName(del)
		m.SetTsig(c.keyname, c.algo, 300, time.Now().Unix())
		_, _, err := cnt.Exchange(m, c.host)
		if err != nil {
			return err
		}
	}
	return nil
}
